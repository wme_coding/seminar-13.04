public enum Grade {
    GOOD(4), VERY_GOOD(5), NEUTRAL(3), NOT_APPROVED(0);

    private int grade;

    Grade(int grade){
        this.grade = grade;
    }

    public int getNumber(){
        return grade;
    }
}
