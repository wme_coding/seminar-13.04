import java.util.ArrayList;
import java.util.List;

public class Сertificate {
    private String studentName;
    private String uniName;
    private String faculty;
    private String speciality;
    private EducationPeriod educationPeriod;
    private List<Line> lines;

    public Сertificate(String studentName, String uniName, String faculty, String speciality, EducationPeriod educationPeriod) {
        this.studentName = studentName;
        this.uniName = uniName;
        this.faculty = faculty;
        this.speciality = speciality;
        this.educationPeriod = educationPeriod;
        this.lines = new ArrayList<>();
    }

    public void addLine(Line line){
        lines.add(line);
    }

    public String getStudentName() {
        return studentName;
    }

    public String getUniName() {
        return uniName;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getSpeciality() {
        return speciality;
    }

    public EducationPeriod getEducationPeriod() {
        return educationPeriod;
    }

    public List<Line> getLines() {
        return lines;
    }
}
