import java.util.Date;

public class EducationPeriod {
    private Date beginningDate;
    private Date endingDate;

    public EducationPeriod(int beginningDay, int beginningMonth, int beginningYear,
                                int endingDay, int endingMonth, int endingYear){
        this.beginningDate = new Date(beginningYear, beginningMonth, beginningDay);
        this.endingDate = new Date(endingYear, endingMonth, endingDay);
    }
}
