import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CertificateService {
    public static List<String> getDisciplines(Сertificate certificate){
        List<String> res = new ArrayList<>();
        for(Line l: certificate.getLines()){
            res.add(l.getDisciplineName());
        }
        return res;
    }

    public static int getCommonComplexity(Сertificate certificate){
        int sum = 0;
        for(Line l: certificate.getLines()){
            sum += l.getComplexity();
        }
        return sum;
    }

    public static double getAverageGrade(Сertificate certificate) throws Exception {
        int sum = 0;
        int neededDisciplines = 0;
        for(Line l: certificate.getLines()){
            if(l.getGrade().getNumber() != 0){
                neededDisciplines++;
                sum += l.getGrade().getNumber();
            }
        }

        if(sum == 0){
            throw new Exception("No approved subjects");
        }

        return sum/neededDisciplines;
    }

    public static Map<String, Grade> subjectGradeMapping(Сertificate certificate){
        Map<String, Grade> res = new HashMap<>();
        for(Line l: certificate.getLines()){
            res.put(l.getDisciplineName(), l.getGrade());
        }
        return res;
    }

    public static Map<Grade, List<String>> gradeSubjectMapping(Сertificate certificate){
        Map<Grade, List<String>> res = new HashMap<>();

        res.put(Grade.VERY_GOOD, new ArrayList<>());
        res.put(Grade.GOOD, new ArrayList<>());
        res.put(Grade.NEUTRAL, new ArrayList<>());
        res.put(Grade.NOT_APPROVED, new ArrayList<>());

        for(Line l: certificate.getLines()){
            res.get(l.getGrade()).add(l.getDisciplineName());
        }

        return res;
    }
}
