public class Line {
    private String disciplineName;
    private int complexity;
    private Grade grade;

    public Line(String disciplineName, int complexity, Grade grade) {
        this.disciplineName = disciplineName;
        this.complexity = complexity;
        this.grade = grade;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public int getComplexity() {
        return complexity;
    }

    public Grade getGrade() {
        return grade;
    }
}
